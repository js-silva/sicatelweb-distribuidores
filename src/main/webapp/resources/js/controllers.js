'use strict';


SicatelWEB.controller( 'HomeController', function ( $scope, $location ) {
	console.log( 'On HomeController  OK!' );
	
	$scope.receptionDG = function( ) {
		$location.path('/receptionDG');
	};
	
	$scope.receptionRDG = function( ) {
		$location.path('/receptionRDG');
	};
	
	
}).controller( 'LoginController', function( $scope, $rootScope, $location, $q, AuthService ) {
	console.log( 'On LoginController  OK!' );
	
	$rootScope.token = "TOKEN";
	$rootScope.user = {
		username: "",
		password: ""
	};
	
	$scope.login = function( ) {
		console.log( 'login function in LoginController  OK!' );
		console.log( '$rootScope.user.username: ' + $rootScope.user.username );
		console.log( '$rootScope.user.password: ' + $rootScope.user.password );
		console.log( '$rootScope.token: ' + $rootScope.token );
		
		AuthService.checkAuthentication( $rootScope.user, $rootScope.token ).then( function ( response ) {
        	console.log( 'response: ' + JSON.stringify( response ) );
        	
            Swal.fire(
                'AuthService',
                'El usuario ha sido autenticado',
                'success'
            );
            
        	$rootScope.authenticated = true;
            $location.path('/home');
		});
	};
	
	
}).controller( 'IndexController', function( ) {
	console.log( 'On IndexController  OK!' );
	
	
}).controller( 'DGController', function( $scope, $rootScope, $location, DGService ) {
	console.log( 'On DGController  OK!' );
	
	$scope.saldo_cobro = "$1750.55";
	$scope.conceptos = [ { id: 'DG', descripcionConcepto: 'DEPÓSITO EN GARANTÍA' } ];
	
	$scope.dgarantia = {
		id: "",
		nombre_titular: "",
		importe: "",
		telefono: "",
		concepto: {
			id: $scope.conceptos[0].id,
			descripcionConcepto: $scope.conceptos[0].descripcionConcepto
		},
		username: "",
		date: "",
		status: ""
	};
	
	$scope.applyDG = function() {
		console.log( 'On applyDG in DGController  OK!' );
		
		$scope.dgarantia.username = $rootScope.user.username;
		
		console.log( "$scope.dgarantia.id: " + $scope.dgarantia.id );
		console.log( "$scope.dgarantia.nombre_titular: " + $scope.dgarantia.nombre_titular );
		console.log( "$scope.dgarantia.importe: " + $scope.dgarantia.importe );
		console.log( "$scope.dgarantia.telefono: " + $scope.dgarantia.telefono );
		console.log( "$scope.dgarantia.concepto.id: " + $scope.dgarantia.concepto.id );
		console.log( "$scope.dgarantia.concepto.descripcionConcepto: " + $scope.dgarantia.concepto.descripcionConcepto );
		console.log( "$rootScope.user.username: " + $rootScope.user.username );
		console.log( "$scope.dgarantia.username: " + $scope.dgarantia.username );
		console.log( "$scope.dgarantia.date: " + $scope.dgarantia.date );
		console.log( "$scope.dgarantia.status: " + $scope.dgarantia.status );
		console.log( "$rootScope.token: " + $rootScope.token )
		
		DGService.applyDG( $scope.dgarantia, $rootScope.token ).then( function ( data ) {
			console.log( JSON.stringify(data) );
			$rootScope.result_applyDG = data.data;
			$location.path('/resultDG');
		});
	};
	
	
}).controller( 'ResultDGController', function( $scope, $rootScope ) {
	console.log( 'On ResultDGController  OK!' );
	
	$scope.dgarantia = $rootScope.result_applyDG;
	$scope.dgarantia.format_date = moment( $scope.dgarantia.date ).format( "YYYY-MM-DD" );
	
	$scope.createPdfDG = function() {
		console.log( 'createPdfDG function in ResultDGController  OK!' );
		
		var docDefinition = {
			pageSize : 'LETTER',
			pageOrientation : 'portrait',
			pageMargins : [ 50, 50, 50, 50 ],
			footer : function(currentPage, pageCount) {
				return {
					text : currentPage.toString() + ' de '
							+ pageCount,
					alignment : 'center'
				};
			},
			content :
			[
				{
					image		: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4RCsRXhpZgAATU0AKgAAAAgABAE7AAIAAAATAAAISodpAAQAAAABAAAIXpydAAEAAAAmAAAQfuocAAcAAAgMAAAAPgAAAAAc6gAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEpPU0UgU0FOQ0hFWiBTSUxWQQAAAAHqHAAHAAAIDAAACHAAAAAAHOoAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEoATwBTAEUAIABTAEEATgBDAEgARQBaACAAUwBJAEwAVgBBAAAA/+EKa2h0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8APD94cGFja2V0IGJlZ2luPSfvu78nIGlkPSdXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQnPz4NCjx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iPjxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+PHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9InV1aWQ6ZmFmNWJkZDUtYmEzZC0xMWRhLWFkMzEtZDMzZDc1MTgyZjFiIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iLz48cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0idXVpZDpmYWY1YmRkNS1iYTNkLTExZGEtYWQzMS1kMzNkNzUxODJmMWIiIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyI+PGRjOmNyZWF0b3I+PHJkZjpTZXEgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj48cmRmOmxpPkpPU0UgU0FOQ0hFWiBTSUxWQTwvcmRmOmxpPjwvcmRmOlNlcT4NCgkJCTwvZGM6Y3JlYXRvcj48L3JkZjpEZXNjcmlwdGlvbj48L3JkZjpSREY+PC94OnhtcG1ldGE+DQogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgIDw/eHBhY2tldCBlbmQ9J3cnPz7/2wBDAAcFBQYFBAcGBQYIBwcIChELCgkJChUPEAwRGBUaGRgVGBcbHichGx0lHRcYIi4iJSgpKywrGiAvMy8qMicqKyr/2wBDAQcICAoJChQLCxQqHBgcKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKir/wAARCABVAckDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD6Roor5q+JGv6paeMZIre8ljTylO0MfVq6cPQdeXKnYzqVORXPpWivj/8A4SjWv+ghN/32aP8AhKNa/wCghN/32a7f7Ml/MYfWl2PsCivj7/hKdZ/6CE3/AH2aP+Ep1n/oITf99mj+zJfzB9aXY+waK+Rn13Xo7GO7fUZBHKxVB5hycdTj0qv/AMJTrP8A0EJv++zR/Zkn9oPrK7H2DRXx9/wlOs/9BCb/AL7NH/CU6z/0EJv++zR/Zkv5g+tLsfYNFfH3/CU6z/0EJv8Avs0v/CUa1/0EJv8Avs0f2ZL+YPrS7H2BRXx//wAJRrX/AEEJv++zR/wlGtf9BCb/AL7NH9mS/mD60ux9gUV8ff8ACU6z/wBBCb/vs0v/AAlGtf8AQQm/77NH9mS/mD60ux9gUV8f/wDCUa1/0EJv++zR/wAJRrX/AEEJv++zR/Zkv5g+tLsfYFFfH/8AwlGtf9BCb/vs0f8ACUa1/wBBCb/vs0f2ZL+YPrS7H2BRXx9/wlOs/wDQQm/77NH/AAlOs/8AQQm/77NH9mS/mD60ux9g0V5J8D9TvNR068a9neYrcEAsc4Gxa9brzq1N0puDOiEueNwooorIsKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACvln4o/wDI7yf9cV/9Cavqavln4o/8jvJ/1xX/ANCavTy3+K/Q5cT8Jx1FFbPh7Q7vVbgzQ6dNe28RxIsRAOT05Ne9KSirs4Urux0Hhfw9cRWLT6h4Wm1IT4aJxIqgL+Jrt9L8GafeWfnXfgueBiThfOXpWNp+nao80cI0TWI4V4O24X5RW1qV/q9sqQWGl6w+Fw26ZOK8qrOUpaP8f+CdUUktSjqukWS3Yit/A9xNHEoUEzIB+HNUG0m2VSzeAZgAMk+en+NNMOsEknRtY5/6eVrnPEms3lmfsWy/srnALCWcN8p+lXCMpOyf4v8AzJk0tf6/Iwdcu7K81N5NNsvsMAAURbs8jrzXqHwt+F9nrWhvq3iKEmKc/wCjoTj5f71c58J9N0XVPEgt9Xglup3OIoguUA7sxr2fxN458PeBlt9Mu1YAx4WKFc7FHApYqtNNUaSdx0oR+OWx8/eOhosXiie18ORbLS3Pl7853sOprm69qsPh5Z+Nb6XXZ4v7J0QAmCPHzyL/AHm9M1mG1+E8d6LXzrp8tt8wD5c1vDExS5Um2t+pEqbbvseUVp+HtEuPEWv2ml2a5e4kAJ/ur1J/AZr1H4hfCvRNB8HXGs6TNKrQ7CEbkOGYD+tdF8JfBUPhrQj4g1UBLuaIt83/ACyj6/nUzxsPZOcfT5hGjLn5WZHj/wAC+FPCPgmW5EH+nMFigy3LOTyfyya8Qr2y50bVfi94kN1Oxs/D9o7LCxHMnuBWo/wt8Bac3lX9+BJ33ygGsqOIVGPLUbcvvsXOm5u8VZHz/RXq3inwV4bn1TTtD8Fn7RqF25aSXzNyxRgck/nWtqHw18F+ENNil8UajI8rjhV4LHvgV0/W6dlo7vp1M/YyPE6K9p8OeE/hz4vuJbPR3uluI034bjj1rgdb8Gva/EaTwzpchuD5iqj46AgE5+maqGJhKTi7prXUl02lc2/hH4HtvFd/eXGrQl7K3UKOwZz/AIDH51lfEq10LTfE7aZ4ch2JajbM+c7n6kfh0r1vxDPb/Cf4aR2ulKGupW2Kx/ic/eavnaaaS4nkmmYvJIxZ2PcnrWOHlKtUdW/u7I0qJQiodT2/4Bf8gu9/6+T/AOgLXZeJdX1m/wDEsXhnwxPFZ3Hkie6vJV3eUhPAUdya434Bf8gu9/6+T/6AtdN4gvf+EP8AiAviC9gmk0y+thbzzRIX8hlPBIHOOleXiUniXf8ArQ6ad1R0/rUXT9G8beGNVtGfW5PE9hNKEuUnjWKSFTxuU7sEDrjrWl4l+IuleGr8WLWmo6neAbnt9NtvNaMercgD86qw/E/Sda1G2sPCKyaxPNIFeVI2SKBe7MzDsOcAc1jx+ItL8EfEDX5PFUjWkd+6SW100TMjLj7uQDzWPLKUvfXfybHeMYtwfbzt5nSt8RNC/wCEV/t+OSaSzWVYZAI9rxMzBfmViMYJ5/rVG3+K+gTXdxBLBqVqY0LwtcWhUXQHaIdWPtgVw1+Pt3hPxBq8NvJBp2o6tbNbRyx7SyiZQW29ga7LxNFGfiJ4JBRcB5sDHT9w9P2cOv8Aw3uphzytddPx1aNLRPiLpOuadqVzFa39rJpsZlntbuARzbQCcgZxzj1rOh+MHh6aa2AtNUW2nwDem0zBExOArOCcH6ZFZ2pADxp40wMf8SM/yNYdl4w0J/hPF4aigmOr3Nv5Mditu25mJwHzjGO+c0RhGSul209bjlJx0b76/d/meg+IvH+l+G7yG1uLa+u5p4DPClnCJDIAQMAZBz82fTAPNVtZ+Juk6N9njaw1W9u5ohK1nZ2vmSwqehcZwPzrJs7N7P4neG7a5+aWHRJQxPODuiqIeINM8EfETxBP4pdrSLUVhe1umiZlYKGBTIBwcnOPekoR0W+/zs7f8EXPLV7bfK6TNPX/ABrb6l8NL7W/Dt3LA8LKrFl2SQtuGVYHocGnaL8TtIvb2002eHUYZJkRY7y4tSkFw+3kK/f8QK4+4f7d4N8Za5b20sem391C1tE6bDIF2hmA9D/StDV/FOk+KNL0jw7oKSvqqzQl4fIZTaBRyWJGB+GaqNOO1uq+V0vyFKbSvfv87P8AU6jxF8SdL8Pam9gNP1XVLmIDzl02083ycjI3EkAcc1PJ8QtEj8Hp4kH2h7IyeWyiPEkbZIIZSRggjmvPtf1y1k8Xaxa+JvEN7oZhkVLWysrc5uk2D594B3Ekke2Ky9MMJ+CN5HEJdi6yw23A/eAF8jcPXBFEaKcE/T8WEqjU2vX8E2el6R8T9G1fXRpa2ep2kkufs8t3amOO5/3DnJ/ECsvwb4/l1nxvr9heWuoRQQzHyXngCxwIqjIY54JIJH1FWvHcaLr3g5lUArqCqCBjA44rHtnFz4i8faHBN5WpXhL28ZUgsPKA3A9OtQlHlckuj/BrUq8rpN9V+N9PwNuP4ueH5NTFv9m1RbNn2DU2tCLUnpnfnOPfGK7oEEZByK+bUfzvDKaNefEDU1mMIifQk0XdIpxgoPmAP1yK+iNPge30m3gd2Z44lUu4wSQOpGT/ADp16caavH/hyaVSU3r/AMMcfrHxS0a0vbvToIdRlaJXR7+C1LW8DgHhn7c98Yo8PeNIdO+GOm6z4iupbiaeMAbU3yzuegVR1JrmtF8T6T4a8K6l4Z1xJhrO6dRbfZ2Y3RbOCpxgj3JFYt3ZXj/DjwZqVvfz6bbWjZmu4Lfz2t8oQH2Z5GTj2zmqVONremvffYcpu9+3Np9x6l4X8eaZ4puJLWG1v9Ou0Xf9m1G38qRl/vAZII/GrXja8uNP8Eatd2UrQ3ENszRyL1U+teb+CEXV/iDYXsPjW/8AExtIpSX/ALM8mJAV27WcsDnJHAB6V6F8Qv8Aknmt/wDXo1ZVoKEkl+v6lUZOW/8AX3HndlaalNpMN9L8YxFI0QkaF/KO04ztPz5/Suz8J+MN/wAPv7a8SXCqsDtGbhUIE4BwGUdea5uX4T+H7/wNZXuj6Xbw6rHClwjHJWZgM7WGehpPE10/ir4T28mjI9hLp93H9rggiDNAVyGwnfGQa3qOMrxXdLZaeZjTUkk327vXTY6nQPiVpGv6qNP+xanps0n+oOoWvlLcf7hyc/jim+IPido2gaq+ntZ6nqEsOPtDWFr5q2+f75yMcc8ZrzrSSmu+JdIij+IGo+IJLe4En2ZNI2CAgH77FhtHUcZ+ldBovibSfAmt+JLHxbI9vcXd+biBmhZxcRsigBSAc4IIwaUqUVLRdNtddvIqNSTjr9+mm/n/AFc7PU/HmiaZoNrqrSzXMV6M20NtEXlm9lX1+tR6F4/0vXrG6mit76zntELy2d7B5Uyr2O3OCD9a4Px1LNf3Ph3xFZXd94a0zyZU8/7EJHtyW4LRg8BuuetV/DUS6jrGo6nF4uvvEoi0542uH07yIsHOF3lskjnjb361Hso+zcuuvyt8ivaS51H0+d/mdbD8ZPDs32eQWmqi1mQM159kzDCT/C7Ann1xmt/xL400vwxaQS3a3F3JcjMFtZRebLL/ALo/+vXmmi+NNAi+Edt4eME39qzWnlpYrbsWlY9HBxgg5znNad6v/CFeJPCup+Imc2FppS2k1xsLiGULgk4H61cqUVO1rav56P8A4b5kxqScb+X3bf8AD/I19W8f6frHgO+1DT7rU9JltnVJk+zAXMBJ6FGYD9a0/G3jGz0DT/sQe+fUbqI+THp9t58yj++FyBx9a5Xxv4q0jxP8PtZl0K3maGN4916YPLjnOR90n5j+IrS1XWbHwj8SH1XxFvhsbuyWOG78susTDGVOASM4qORaJq2r0+SK52tU7/8ADmvpvj3Rz4JudYR76aPTYs3UU8YW5UgchlJAz+OKrWPxY0G+1q3sBa6pBHdOEgvZ7QpBKx6KGznJ+mK47V7qLXNB8da9pkEiaXcWPlwyuhTzmC8sB6e9dH44ijXwP4fKoo2ahZbcDGP3qVXJHmXMt2l6XE5SS917J/Ox6NRSJ/q1+lLXIdAV8s/FH/kd5P8Ariv/AKE1fU1fLPxR/wCR3k/64r/6E1enlv8AFfocuJ+E48Ak4Ayewru9E/sKx02NTq+s2k7jMyQW/wAu7865nQYIDeC5ubv7KYSGjJgMoY/TIruf+EunHTXbQf8AcH/+yr1qzb91f1+DOWFlqzes7nRNN09ph4q1rc43FWg5+lYMmpaJLIzt4j8QZY5P+j//AF61rfxo2o2qWLazYQyjkXD6Uefr81Zb+LLiORkOu2mVJHGj/wD2VcUYyu7rX+v7ps2rf1/mQyajoSRsx8Sa+MDqYAB/OvPb+6kvb6WaWeS4LMQrynLFe2fwrpfE3i++vYG0+O+trq3kX940dkIWB9Opq58LfA58Xa8Zrk7bGyZWlGOXP92uuLVKDqT/AK/BGTvOXKj0n4UeF4/CnhGbxFqqhbi4iMvzDmOPGR+Y5/GvP/DenXfxU+JE19qO4Wgk82UdlTPyoPw4r1H4svrZ8LnTNAsWa3kj/wBImUgCNB/Dj6U74NaFDpXgO3vFIefUB5zsOwPQfgK81VnGnKu/ilovI6eS8lDojpPEuiTar4Ym0jTLpLASp5e8LnanQgD6V5nYfASO31G3mudYWWGOVXeMRY3AHJHWk8ReHPibqPiK9ubO68m2eU+VHHLwEzx29K5XxDa/EHwxZJc6tqsqJI4RFWQFmJ9BinQhOMeWFRainKLd5Reh6f4xuk1/xjovg+2HmQLKt1e46CNPmCn6kCtPxfM+qXdp4Q01thu/mu3X/llbryfxbhfxrH8H6O/gjwnf+KfEsrXOoywedKzfeVQMhM+pOKvfDHzNYs7zxTekNdanIQq/88ox0UVzytFXjtH8WaK7dn1/Iq/EfxZa+BPCaaVo4WO9nTy4FX/lmvdj/nvXzpc3lzdzNNdXEssjHJZnNet+PPhz4v8AEvjC7v0iikgJ2wfvOiVyafDjU9O8VaPpuueVCL6faAHydqjJ/lj8a9LCOjTp73b1ZzVeeUttD0n4J+Df7N0x/EOoA/abtdkIb+CPufxP8qm8YfCi98X6/JqN3rion3YofLyI1/Oup8aaXq7+Chp3hIiG5VkRSDt2oOuPyFeWf8If8VP+ghJ/39H+FcdOcqk3W51F+ZtJKMVDludd4Y8EWfwtstV129vlunEGFIXbtAzx17nH5VX+E2gy6heah421hcT30rfZ1YfdT1/p+FcHp+l+KNd8ap4T1vVpZE4e6UMGUKOcdPpXpnxJ8XW3gXwtDo2lqBdTw+XCoP8Aq0HG41VSM78id5T6+QouNua1kvzPMvjB4v8A+Ei8UmxtWzZafmNSD99/4j/T8K88pWYsxZyWZjkk9zSV7FKmqcFBdDjlJyd2e5fAL/kF3v8A18n/ANAWvY5I0mjKSoroeqsMg1458Av+QXe/9fJ/9AWvZa+dxv8AHkejQ/hor21hZ2WfsdpBb7uvlRhc/lS3NjaXqgXlrDcAdBLGGx+dT0VyXZsRPbQSQiGSCNohjCMgKjHTilaCF5EkeJGeP7jFQSvbg9qkopAZutaWL/R9Rgto4lurq2eESMMZJUgZIGcVV8MaAmkaDp9vewWz3trCI2mRQT+DEZxW5RVKTSt/X9aiaTaf9dP8iMwRGcTGJDKoKiQqNwHpmmXNla3qhby2huApyBLGGx+dT0VIyM20DQCBoYzEOBGUG38qZHY2kM7TQ2sMcrDDSLGAx+pqeii4FebT7K4nWa4tIJZV+7I8Ssw/EilNhaGNkNrBsdt7L5YwzepHrU9FO7AjkghmZGliRzGcoWUHafUelJ9kt/tX2nyIvPxjzdg3Y9M9alopAV/7OsvtX2n7Hb/aP+evlLu/PGasUUUAQSWNpLcCeW1heZRgSNGCw/GnLbQLb/Z1gjEOMeWEG3HpjpUtFAENtZWtmpWztoYFPJEUYXP5VJJGk0bRzIsiMMMrDII+lOooAREWNAqKFVRgADAFRx20EO/yoY08w5fagG4+p9alooAr29hZ2kjPa2kEDv8AeaOMKW+pFFxYWd26vdWsE7J90yRhiv0zViindgMkgimhMUsSSRkYKMoI/KqV3pcR0a5stPght/NjZVVECLkj2FaFFLdWBaO5geE/Di6L4Z0u01CC2kvrO3WJpkUNyPRiAcVtz28NzEY7mGOaM9UkUMD+BqSiqlJybkyYxUVZEAsLQWv2YWsAg/55eWNv5dKdcWtvdx+XdQRTp/dkQMPyNS0VNyiH7JbfZfs32eLyMY8rYNuPTHSnSW8EsapLDG6KQVVlBAI6ECpKKLgFFFFABXyz8Uf+R2l/64r/AOhNX1NXzt8QvA/ibU/Fslzp+jXVxCYlAdFyM5b/ABr0svlGNRuTsc2ITcdDltO8e6zpVhFZ2cen+TEML5lorMfqT1qz/wALN8Q/889M/wDAFag/4Vv4y/6F68/74/8Ar0f8K38Zf9C9ef8AfH/169d/V27u34HJ+88yf/hZviH/AJ56Z/4ApUtz8U9en2bLbS4tqBWxZKdx/vc1T/4Vv4y/6F68/wC+P/r0f8K38Zf9C9ef98f/AF6VsN5fgF6nmc5PO9zcyTzEGSVi7EDAyfaur8D/ABFv/Ay3MdnaQXUVywZlkJBBAxwRVf8A4Vv4y/6F68/74/8Ar0f8K38Zf9C9ef8AfH/16ucqM48smreokpxd0jptd+N2ra3od1po023thcxmNpUckqD1xWL4S+KOveEbQWdr5N3ZqflhnB+T6EEVT/4Vv4y/6F68/wC+P/r0f8K38Zf9C9ef98f/AF6yUMKo8mlvUrmqt31O2/4aD1X/AKAlr/39auX1v4l3niHxPYatqWnwyQ2DBorPedhYdye/NUf+Fb+Mv+hevP8Avj/69H/Ct/GX/QvXn/fH/wBelGnhYO8bfeDlVe5s+Lfi7qnizQX0l7GGzhkZTI0bklgDnHPuBVDwh8Tdc8HW5tbMQ3VmTkQzg/KfYg8VV/4Vv4y/6F68/wC+P/r0f8K38Zf9C9ef98f/AF6pRwyhyaW9RXq35tbnbf8ADQeq/wDQEtf+/rVwXiPxlqfiPxMutzP9nniI8hYzxFj0qf8A4Vv4y/6F68/74/8Ar0f8K38Zf9C9ef8AfH/16UIYam7xt945SqyVnc6rTvjz4gtLVYr2ws7xlGPM+ZCfrzVmX9oDV3hdU0e1RmUgN5jHB9a4z/hW/jL/AKF68/74/wDr0f8ACt/GX/QvXn/fH/16l0cI3fT7x89bzLXhP4h3HhbVL/Ujp8V/fXzZeeVyCo64GP8APArG8UeJbzxZr0uqahhXcBUjU/LGo6Afqfxq/wD8K38Zf9C9ef8AfH/16P8AhW/jL/oXrz/vj/69ar2Clzpq/qQ/aNWME3EBslhW1USjrNuOTz6dKrV0/wDwrfxl/wBC9ef98f8A16P+Fb+Mv+hevP8Avj/69ae1pL7S+8nll2PS/gF/yC73/r5P/oC17LXlnwZ8O6voOn3aazYTWbPcFlEgxkbAM16nXzuMadZtHpUU1BXCiiiuQ1CiiigAooooAKKKKACiiigAqreajb2ccheWMyohcRFwGbA9KnmYpC7KMlVJAryPR/CeleJ/DepeKNaluDrLT3IFwLl1NsEkZVVVBwBhRwQapJNNvoJvVJdT0jw5r8ev+GLTWXiFolym/Y8mdnOPvYHpWnDcQ3Me+3ljlT+8jBh+leC3F9eP4J8GaTHp15qtncW7vNaWk/ktcYJ4LDnHsPWuh+HkWo6Z40FrY+FL/wAO6PNATLbXN0ZkDjoy7uRmumVBXk152+Rzqtok/L8T0Txb4hHhbwxd6w1sboWybvKD7N344OPyrkR8SvEsKefqPw71G2s1G6WdbpX2L3O3aM8Vp/FzP/CsNXx18qscD4salYmyuLbw5ZW88fltcRGR3RSMZALYJrOnFODbS36uxpUbUkrv5HoFjqtnqGkwalBMotZ4xIjuQBgjPNTwXMFypa2mjmUdTG4YfpXmXizRrHTtF0HwlDbXuqyqhWOxjufJScKpBMrAZ298Aiue8LJqHhPxhrFta6bFpMS6PLcpYQXTzrvUAhjvJwafs4tSafe3yEpyTimtdL/M9sa+tEkaN7qFXUZZTIAQPcU97iGKHzZJUSP++zAD868Y1TwDoM/wruvEctxcNrElqZnv/tT7nPdCM7cHpjFatxp8HinxJ4f8O608p0kaU1ybdJWjW4kBQAMVIJADE4zR7JXtfbf5K/6B7R2vbR/5pfqepxTxTx+ZBKkif3kYEfmKZHeW00rRw3EUki9UVwSPwry+DRf+Ed8W6t4Y8LXklpZXekPMkckxdbWUMqggscrkMe9c7oVlb+FPEWnza54bv9JuPOCtrFpfPJFdMeAHV8gg5zxiiNKMtnvt/V/IJVJR3W2/3J/qe8UUA5AI6Giuc2CiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigArir74W6Ne6zPepd6jaw3LF7ixt7plt5XPVmT1/Su1opqTi7oTSaszlJfh3pE3hWy0N5LoLYDFtdxy+XNEfUMuKk8MeBLDwzcvd/bdQ1S+Zdn2vUbkyuF9B2A+grp6Kr2k9ddyeSOmmxl+JNAt/E2gXOk3kssUNwu1niIDD6ZBFaUaCONUHRRgZp1FTd2sVbW5znivwXY+LEt2nurywurY5hu7GYxypnqAfeszR/htpnhnUX1mzn1C/vxbPFL9quPMN1kdGLA4P0wK7aiqU5KPKnoJxi3do+c9TstB1YXWkaZB4mg1K4O2PRHlf7JDIT976Dr6V7BqngKx17RNOgvZrqzvbKMLFeWcxjliOMEAjsfeusorSVZtJLoRGmk22crofw80bRbC9gkNzqUt/H5d1dX0xlllXsCe3Xtis/T/hRpVlqUdxPqusX9tCweGwu7xngiI6EL7ds5ruqKj2s73uV7OFrWDp0ooorMsKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP/2Q==',
					alignment	: 'center',
					width		: 250,
					height		: 50
				},
				{
					text		: '\n\n' + 'DEPOSITO EN GARANTÍA COBRADO POR CUENTA Y ORDEN DE' + '\n' + 'RADIOMOVIL DIPSA S.A. DE C.V.' + '\n\n\n\n',
					alignment	: 'center',
					bold		:	true,
					color		: '#000000'
				},
				{
					text		: 'DATOS DEL DISTRIBUIDOR' + '\n' + '-------------------------------------' + '\n',
					alignment	: 'left',
					bold		:	true,
					color		: '#000000'
				},
				{
					text		: 'Nombre:                DISTRIBUIDOR DE PRUEBAS' + '\n',
					alignment	: 'left',
					bold		:	true,
					color		: '#000000'
				},
				{
					text		: 'Punto de Ventas: DELTA01' + '\n',
					alignment	: 'left',
					bold		:	true,
					color		: '#000000'
				},
				{
					text		: 'Dirección:             Av. Cuauhtémoc 462, Piedad Narvarte, Benito Juárez, 03020, CDMX' + '\n\n',
					alignment	: 'left',
					bold		:	true,
					color		: '#000000'
				},
				{
					text		: 'TRANSACCIÓN' + '\n' + '-------------------------' + '\n',
					alignment	: 'center',
					bold		:	true,
					color		: '#000000'
				},
				{
					columns:
					[
						{
							text		: 'FECHA:',
							alignment	: 'left',
							bold		: true,
							color		: '#000000',
							width		: '25%'
						},
						{
							text		: $scope.dgarantia.format_date,
							alignment	: 'left',
							bold		: false,
							color		: '#000000',
							width		: '75%'
						}
					],
					columnGap: 10
				},
				{
					columns:
					[
						{
							text		: 'TELEFONO:',
							alignment	: 'left',
							bold		: true,
							color		: '#000000',
							width		: '25%'
						},
						{
							text		: $scope.dgarantia.telefono,
							alignment	: 'left',
							bold		: false,
							color		: '#000000',
							width		: '75%'
						}
					],
					columnGap: 10
				},
				{
					columns:
					[
						{
							text		: 'NOMBRE:',
							alignment	: 'left',
							bold		: true,
							color		: '#000000',
							width		: '25%'
						},
						{
							text		: $scope.dgarantia.nombre_titular,
							alignment	: 'left',
							bold		: false,
							color		: '#000000',
							width		: '75%'
						}
					],
					columnGap: 10
				},
				{
					columns:
					[
						{
							text		: 'MONTO:',
							alignment	: 'left',
							bold		: true,
							color		: '#000000',
							width		: '25%'
						},
						{
							text		: $scope.dgarantia.importe,
							alignment	: 'left',
							bold		: false,
							color		: '#000000',
							width		: '75%'
						}
					],
					columnGap: 10
				},
				{
					columns:
					[
						{
							text		: 'CONCEPTO:',
							alignment	: 'left',
							bold		: true,
							color		: '#000000',
							width		: '25%'
						},
						{
							text		: $scope.dgarantia.concepto.descripcionConcepto,
							alignment	: 'left',
							bold		: false,
							color		: '#000000',
							width		: '75%'
						}
					],
					columnGap: 10
				},
				{
					columns:
					[
						{
							text		: 'FOLIO:',
							alignment	: 'left',
							bold		: true,
							color		: '#000000',
							width		: '25%'
						},
						{
							text		: $scope.dgarantia.id,
							alignment	: 'left',
							bold		: false,
							color		: '#000000',
							width		: '75%'
						}
					],
					columnGap: 10
				},
				{
					columns:
					[
						{
							text		: 'RESULTADO:',
							alignment	: 'left',
							bold		: true,
							color		: '#000000',
							width		: '25%'
						},
						{
							text		: ($scope.dgarantia.status === 'OK' ) ? 'EXITOSO' : 'FALLIDO',
							alignment	: 'left',
							bold		: false,
							color		: '#000000',
							width		: '75%'
						}
					],
					columnGap: 10
				}
			]
		};
		
		pdfMake.createPdf(docDefinition).open();
	};
	
	
}).controller( 'RDGController', function( $scope ) {
	console.log( 'On RDGController  OK!' );
	
	$scope.requestRDG = function(  ) {
		console.log( 'requestRDG function in RDGController  OK!' );
	};
	
	
}).controller( 'DateTimePickerController', function( $scope ) {
	console.log( 'On DateTimePickerController  OK!' );
	
	
	$scope.today = function() {
	$scope.dt = new Date();
	};
	$scope.today();

	$scope.clear = function() {
		$scope.dt = null;
	};

	$scope.inlineOptions = {
		customClass : getDayClass,
		minDate : new Date(),
		showWeeks : true
	};

	$scope.dateOptions = {
		dateDisabled : disabled,
		formatYear : 'yy',
		maxDate : new Date(2020, 5, 22),
		minDate : new Date(),
		startingDay : 1
	};

	// Disable weekend selection
	function disabled(data) {
		var date = data.date, mode = data.mode;
		return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
	}
	
	$scope.toggleMin = function() {
		$scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
		$scope.dateOptions.minDate = $scope.inlineOptions.minDate;
	};
	
	$scope.toggleMin();
	
	$scope.open1 = function() {
		$scope.popup1.opened = true;
	};
	
	$scope.open2 = function() {
		$scope.popup2.opened = true;
	};
	
	$scope.setDate = function( year, month, day ) {
		$scope.dt = new Date(year, month, day);
	};
	
	$scope.formats = [ 'dd-MMMM-yyyy', 'yyyy/MM/dd',
			'dd.MM.yyyy', 'shortDate' ];
	$scope.format = $scope.formats[0];
	$scope.altInputFormats = [ 'M!/d!/yyyy' ];

	$scope.popup1 = {
		opened : false
	};

	$scope.popup2 = {
		opened : false
	};
	
	var tomorrow = new Date();
	tomorrow.setDate(tomorrow.getDate() + 1);
	var afterTomorrow = new Date();
	afterTomorrow.setDate(tomorrow.getDate() + 1);
	$scope.events = [ {
		date : tomorrow,
		status : 'full'
	}, {
		date : afterTomorrow,
		status : 'partially'
	} ];

	function getDayClass(data) {
		var date = data.date, mode = data.mode;
		if (mode === 'day') {
			var dayToCheck = new Date(date).setHours(0, 0, 0, 0);
			
			for (var i = 0; i < $scope.events.length; i++) {
				var currentDay = new Date($scope.events[i].date)
						.setHours(0, 0, 0, 0);
				
				if (dayToCheck === currentDay) {
					return $scope.events[i].status;
				}
			}
		}
		return '';
	};
	
	
});
