'use strict';


SicatelWEB.service( 'AuthService', function ( $http, $httpBackend, $cacheFactory, $injector ) {
	console.log( 'On AuthService  OK!' );
	
    return {
    	checkAuthentication: function ( user, token ) {
    		var reqAuth = {
    			headers: {
    				'Content-Type': 'application/json',
    				'token': token,
    				'Accept': '*/*'
    			}
    		};
    		
    		return $http.post( 'user/auth', {'username': user.username,'password': user.password}, reqAuth );
        }
    }
}).service( 'DGService', function( $http ) {
	console.log( 'On DGService  OK!' );
	
    return {
    	applyDG: function ( dg, token ) {
    		var reqAuth = {
    			headers: {
    				'Content-Type': 'application/json',
    				'token': token,
    				'Accept': '*/*'
    			}
    		};
    		
    		return $http.post( 'dgarantia/apply', { "id": dg.id, "nombre_titular": dg.nombre_titular, "importe": dg.importe, "telefono": dg.telefono, "concepto": { "id":dg.concepto.id, "descripcionConcepto": dg.concepto.descripcionConcepto}, "username": dg.username, "date": dg.date, "status": dg.status }, reqAuth );
        }
    }
});
