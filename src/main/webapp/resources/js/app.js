'use strict';


var SicatelWEB = angular.module( 'SicatelWEB', [ 'ngResource','ngRoute', 'ngAnimate', 'ngTouch', 'ngSanitize', 'ngAria', 'ui.bootstrap', 'ui.grid', 'ui.grid.exporter', 'ui.grid.selection' ] );


SicatelWEB.constant('USER_ROLES', {
	ALL : '*',
	ODG : 'ODG',
	OPF : 'OPF',
	RDG : 'RDG',
	RPF : 'RPF',
	MDG : 'MDG',
	MPF : 'MPF'
});


SicatelWEB.config(function($routeProvider, USER_ROLES) {
	$routeProvider.when('/home', {
		templateUrl	: 'partials/home.html',
		controller	: 'HomeController',
		access: {
			loginRequired	: true,
			authorizedRoles	: [USER_ROLES.ALL, USER_ROLES.ODG, USER_ROLES.OPF, USER_ROLES.RDG, USER_ROLES.RPF, USER_ROLES.MDG, USER_ROLES.MPF]
		}
	}).when('/login', {
		templateUrl : 'partials/login.html',
		controller	: 'LoginController',
		access: {
			loginRequired	: false,
			authorizedRoles	: []
		}
	}).when( '/receptionDG', {
		templateUrl : 'partials/recepcion-dg.html',
		controller	: 'DGController',
		access: {
			loginRequired: true,
			authorizedRoles : [ USER_ROLES.ALL, USER_ROLES.ODG ]
		}
	}).when( '/resultDG', {
		templateUrl : 'partials/resultado-dg.html',
		controller	: 'ResultDGController',
		access: {
			loginRequired: true,
			authorizedRoles : [ USER_ROLES.ALL, USER_ROLES.ODG ]
		}
	}).when( '/receptionRDG', {
		templateUrl : 'partials/recepcion-rdg.html',
		controller	: 'RDGController',
		access: {
			loginRequired: true,
			authorizedRoles : [ USER_ROLES.ALL, USER_ROLES.RDG ]
		}
	}).when( '/resultRDG', {
		templateUrl : 'partials/resultado-rdg.html',
		controller	: 'ResultRDGController',
		access: {
			loginRequired: true,
			authorizedRoles : [ USER_ROLES.ALL, USER_ROLES.RDG ]
		}
	});
});


SicatelWEB.run(function ( $rootScope, $location, $route ) {
	console.log( "RUN OK!!" );
	console.log( $route );
	
	$rootScope.authenticated = false;
	if( !$rootScope.authenticated ) {
        $location.path("/login");
    };
    //GESTIONAR: LOGIN REQUERIDO
	//GESTIONAR: AUTORIZACION
});