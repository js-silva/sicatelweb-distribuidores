package mx.telcel.poc.distribuidores_prototype.initializer;


import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import mx.telcel.poc.distribuidores_prototype.config.MvcConfig;
import mx.telcel.poc.distribuidores_prototype.config.ServiceConfig;
import mx.telcel.poc.distribuidores_prototype.utils.Constants;


public class MvcWebApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
	
	
	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { ServiceConfig.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[] { MvcConfig.class };
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { Constants.__SERVLET_CONTEXT_MAPPING__ };
	}

}
