package mx.telcel.poc.distribuidores_prototype.utils;


public interface Constants {
	
	
	public		static		final		String		__SERVLET_CONTEXT_MAPPING__				=		"/";
	public		static		final		String		__SERVICE_BASE_SCAN__					=		"mx.telcel.poc.distribuidores_prototype.services";
	public		static		final		String		__CONTROLLER_BASE_SCAN_01__				=		"mx.telcel.poc.distribuidores_prototype.controllers";
	public		static		final		String		__CONTROLLER_BASE_SCAN_02__				=		"mx.telcel.poc.distribuidores_prototype.controllers.simulator";
	public		static		final		String		__MESSAGE_SOURCE__						=		"messageSource";
	public		static		final		String		__ERROR_PROTOTYPE_MESSAGE_01__			=		" CLASS NOT AUTH FOR USE USER CLASS";
	public		static		final		String		__ERROR_PROTOTYPE_MESSAGE_02__			=		"USER CAN'T BE NULL OR EMPTY";
	public		static		final		String		__ERROR_PROTOTYPE_MESSAGE_03__			=		"AUTHENTICATION RESULT CAN'T BE NULL";
	public		static		final		String		__ERROR_PROTOTYPE_MESSAGE_04__			=		"AUTORIZATION CAN'T BE NULL OR EMPTY";
	public		static		final		String		__PATH_USER_AUTH_SIMULATOR_SERVICE__	=		"/user/auth";
	
	public		static		final		Long		__NUMBER_1_LONG__						=		1L;
	
	public		static		final		Integer		__NUMBER_1_INT__						=		new Integer(1);
	public		static		final		Integer		__NUMBER_2_INT__						=		new Integer(2);
	
}
