package mx.telcel.poc.distribuidores_prototype.utils;


import java.util.HashMap;

import mx.telcel.poc.distribuidores_prototype.beans.DG;


public class DGs {
	
	
	private DGs() {
		super();
	}
	
	
	private	static HashMap<String, DG>	dgMap = null;
	
	
	public synchronized static HashMap<String , DG> getDGMap() {
		if( DGs.dgMap == null ) {
			DGs.dgMap = new HashMap<String , DG>();
		}
		
		return DGs.dgMap;
	}
	
}
