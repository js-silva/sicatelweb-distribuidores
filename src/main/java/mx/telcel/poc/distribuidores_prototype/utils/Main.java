package mx.telcel.poc.distribuidores_prototype.utils;


import java.io.File;
import java.io.InputStreamReader;
import java.util.Base64;
import java.util.UUID;

import javax.imageio.stream.FileImageInputStream;


public class Main {
	
	
	public Main() {
		super();
	}
	
	/*
	public static void main( String[] args ) {
		UUID id = UUID.randomUUID();
		System.out.println( id.toString() );
	}
	*/
	
	public static void main( String[] args ) throws Exception {
		//OPEN THE FILE
		File theImage = new File( "C:\\Users\\VI9XXRG\\Desktop\\imagenTelcelDG001.jpg" );
		FileImageInputStream fis = new FileImageInputStream(theImage);
		
		//BUFFER
		long lLength = fis.length();
		System.out.println( "lLength: " + lLength );
		byte[] contentAsByteA = new byte[ (new Long(lLength)).intValue() ];
		
		//GET STREAM
		fis.read( contentAsByteA, 0, (new Long(lLength)).intValue() );
		
		//ENCODE BASE64
		byte[] contentAsByteAB64 = Base64.getEncoder().encode( contentAsByteA );
		System.out.println( "lLength B64: " + contentAsByteAB64.length );
		
		//TRANSFORM
		String contentAsStringB64 = new String( contentAsByteAB64 );
		
		//PRINT
		System.out.println( "contentAsStringB64: " + contentAsStringB64 );
		
		//CLOSE ARCHIVO
		fis.close();
	}
	
}
