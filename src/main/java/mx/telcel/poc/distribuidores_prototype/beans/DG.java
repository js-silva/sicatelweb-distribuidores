package mx.telcel.poc.distribuidores_prototype.beans;

import java.util.Date;

public class DG {
	
	
	public DG() {
		this.id	= null;
		this.nombre_titular = null;
		this.importe = null;
		this.telefono = null;
		this.concepto = null;
		this.username = null;
		this.date = null;
		this.status = null;
	}
	
	
	private		String		id;
	private		String		nombre_titular;
	private		String		importe;
	private		String		telefono;
	private		Concepto	concepto;
	private		String		username;
	private		Date		date;
	private		String		status;
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getNombre_titular() {
		return nombre_titular;
	}
	public void setNombre_titular(String nombre_titular) {
		this.nombre_titular = nombre_titular;
	}
	
	public String getImporte() {
		return importe;
	}
	public void setImporte(String importe) {
		this.importe = importe;
	}
	
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	public Concepto getConcepto() {
		return concepto;
	}
	public void setConcepto(Concepto concepto) {
		this.concepto = concepto;
	}
}
