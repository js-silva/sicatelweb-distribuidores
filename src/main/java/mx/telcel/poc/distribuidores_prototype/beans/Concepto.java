package mx.telcel.poc.distribuidores_prototype.beans;


public class Concepto {
	
	
	public Concepto() {
		this.id = null;
		this.descripcionConcepto = null;
	}
	
	
	private		String		id;
	private		String		descripcionConcepto;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getDescripcionConcepto() {
		return descripcionConcepto;
	}
	public void setDescripcionConcepto(String descripcionConcepto) {
		this.descripcionConcepto = descripcionConcepto;
	}
}
