package mx.telcel.poc.distribuidores_prototype.beans;


import mx.telcel.poc.distribuidores_prototype.controllers.simulator.UserAuthService;
import mx.telcel.poc.distribuidores_prototype.exceptions.PrototypeException;
import mx.telcel.poc.distribuidores_prototype.utils.Constants;


public class User {
	
	
	private String		username		= null;
	private Boolean		authentication	= false;
	private	String[]	autorization	= null;
	
	
	public User() {
		super();
		
		this.username		= null;
		this.authentication	= false;
		this.autorization	= null;
	}
	
	
	public User createUser ( Class<?> className, String username, Boolean authentication, String[] autorization ) throws PrototypeException {
		if( className != UserAuthService.class ) {
			throw new PrototypeException( className.getCanonicalName() + Constants.__ERROR_PROTOTYPE_MESSAGE_01__ );
		}
		
		if( username == null || username.isEmpty() ) {
			throw new PrototypeException( Constants.__ERROR_PROTOTYPE_MESSAGE_02__ );
		}
		this.username = username;
		
		if( authentication == null ) {
			throw new PrototypeException( Constants.__ERROR_PROTOTYPE_MESSAGE_03__ );
		}
		this.authentication = authentication;
		
		if( autorization == null || autorization.length == 0 ) {
			throw new PrototypeException( Constants.__ERROR_PROTOTYPE_MESSAGE_04__ );
		}
		this.autorization = autorization;
		
		return this;
	}
	
	
	public String getUserName() {
		return username;
	}
	
	public boolean isAuthenticatedUser() {
		return authentication;
	}
	
	public String[] getAutorization() {
		return autorization;
	}
	
	
}
