package mx.telcel.poc.distribuidores_prototype.beans;

public class Error {
	
	
	public Error() {
		super();
		
		this.code = null;
		this.description = null;
		this.element = null;
	}
	
	
	private	String	code;
	private	String	description;
	private	String	element;
	
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getElement() {
		return element;
	}
	public void setElement(String element) {
		this.element = element;
	}
}
