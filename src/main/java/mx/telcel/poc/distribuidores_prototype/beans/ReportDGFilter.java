package mx.telcel.poc.distribuidores_prototype.beans;


import java.util.Date;


public class ReportDGFilter {
	
	
	public ReportDGFilter() {
		super();
		
		this.username = null;
		this.rangeStart = null;
		this.rangeEnd =  null;
		this.status = null;
	}
	
	
	private		String		username;
	private		Date		rangeStart;
	private		Date		rangeEnd;
	private		String		status;
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public Date getRangeStart() {
		return rangeStart;
	}
	public void setRangeStart(Date rangeStart) {
		this.rangeStart = rangeStart;
	}
	
	public Date getRangeEnd() {
		return rangeEnd;
	}
	public void setRangeEnd(Date rangeEnd) {
		this.rangeEnd = rangeEnd;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
