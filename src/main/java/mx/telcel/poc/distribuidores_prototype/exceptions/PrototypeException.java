package mx.telcel.poc.distribuidores_prototype.exceptions;


import mx.telcel.poc.distribuidores_prototype.utils.Constants;


public class PrototypeException extends Exception {
	
	
	/**
	 *SERIAL VERSION ID
	 */
	private static final long serialVersionUID = Constants.__NUMBER_1_LONG__;
	
	
	public PrototypeException() {
		super();
	}
	
	public PrototypeException(String message) {
		super(message);
	}
	
	public PrototypeException(Throwable cause) {
		super(cause);
	}
	
	public PrototypeException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public PrototypeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
