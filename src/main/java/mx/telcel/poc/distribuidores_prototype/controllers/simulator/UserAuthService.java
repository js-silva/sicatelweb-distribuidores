package mx.telcel.poc.distribuidores_prototype.controllers.simulator;


import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mx.telcel.poc.distribuidores_prototype.beans.RequestUser;
import mx.telcel.poc.distribuidores_prototype.beans.User;
import mx.telcel.poc.distribuidores_prototype.exceptions.PrototypeException;
import mx.telcel.poc.distribuidores_prototype.utils.Constants;


@RestController
public class UserAuthService {
	
	
	@RequestMapping(  method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE }, path = "/user/auth" )
	public @ResponseBody  User checkAuthToUser( @RequestBody RequestUser user, @RequestHeader String token ) throws PrototypeException {
		
		
		Boolean		authentication	= new Boolean( false );
		String[] 	autorization	= new String[ Constants.__NUMBER_1_INT__.intValue() ];
					autorization[0] = null;
		
		
		//AUTH SIMULATION SERVICE
		if( user.getUsername().equals( "EX310407" ) && user.getPassword().equals( "password" ) ) {
			authentication = new Boolean( true );
			autorization[0] = "ODG|OPF|RDG|RPF|MDG|MPF";
		}
		
		if( user.getUsername().equals( "PDVDG001" ) && user.getPassword().equals( "password" ) ) {
			authentication = new Boolean( true );
			autorization[0] = "ODG|MDG";
		}
		
		if( user.getUsername().equals( "PDVDG002" ) && user.getPassword().equals( "password" ) ) {
			authentication = new Boolean( true );
			autorization[0] = "RDG";
		}
		
		if( user.getUsername().equals( "PDVPF001" ) && user.getPassword().equals( "password" ) ) {
			authentication = new Boolean( true );
			autorization[0] = "OPF|MPF";
		}
		
		if( user.getUsername().equals( "PDVPF002" ) && user.getPassword().equals( "password" ) ) {
			authentication = new Boolean( true );
			autorization[0] = "RPF";
		}

		
		User userForReturn = new User();
		return userForReturn.createUser( this.getClass(), user.getUsername(), authentication, autorization ) ;
	}
	
	
}
