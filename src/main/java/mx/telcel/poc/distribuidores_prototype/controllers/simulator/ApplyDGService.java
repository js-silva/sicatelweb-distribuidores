package mx.telcel.poc.distribuidores_prototype.controllers.simulator;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;
import java.util.UUID;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mx.telcel.poc.distribuidores_prototype.beans.DG;
import mx.telcel.poc.distribuidores_prototype.beans.Error;
import mx.telcel.poc.distribuidores_prototype.beans.ReportDGFilter;
import mx.telcel.poc.distribuidores_prototype.exceptions.PrototypeException;
import mx.telcel.poc.distribuidores_prototype.utils.DGs;


@RestController
public class ApplyDGService {
	
	
	@RequestMapping(  method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE }, path = "/dgarantia/apply" )
	public @ResponseBody ResponseEntity<?> applyDGarantia
	(
		@RequestBody ( required = true ) DG dg,
		@RequestHeader (value="token", required = true)	String token
	)
	{
		try {
			dg.setId( UUID.randomUUID().toString() );
			dg.setDate( new Date() );
			dg.setStatus( (new Random()).nextInt() % 2 == 0 ? "OK" : "KO" );
			
			//SAVE IN MEMORY
			DGs.getDGMap().put( dg.getId(), dg );
			
			//RETURN DG
			return new ResponseEntity<DG>( dg, HttpStatus.OK );
		} catch( Exception e ) {
			Error 	error	= new Error();
			error.setCode( HttpStatus.INTERNAL_SERVER_ERROR.toString() );
			error.setDescription( "INTERNAL_SERVER_ERROR: " + e.getMessage() );
			error.setElement( ApplyDGService.class.getCanonicalName() );
			
			return new ResponseEntity<Error>( error, HttpStatus.INTERNAL_SERVER_ERROR );
		}
	}
	
	@RequestMapping(  method = RequestMethod.GET, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE }, path = "/dgarantia/all" )
	public @ResponseBody ResponseEntity<?> getAll
	(
		@RequestHeader (value="token", required = true)	String token
	)
	throws PrototypeException
	{
		try {
			ArrayList<DG>	dgs = new ArrayList<DG>();
			
			//LOAD FROM MEMORY IN ARRAYLIST
			if( DGs.getDGMap().isEmpty() ) {
				throw new PrototypeException( "NO HAY REGISTROS DE DEPÓSITOS EN GARANTÍA" );
			}
			Collection<DG> colDGs = DGs.getDGMap().values();
			Iterator<DG> iDGs = colDGs.iterator();
			while( iDGs.hasNext() ) {
				dgs.add( iDGs.next() );
			}
			
			//RETURN ARRAYLIST DG
			return new ResponseEntity<ArrayList<DG>>( dgs, HttpStatus.OK );
			
		} catch( PrototypeException pE ) {
			Error 	error	= new Error();
			error.setCode( HttpStatus.INTERNAL_SERVER_ERROR.toString() );
			error.setDescription( "INTERNAL_SERVER_ERROR: " + pE.getMessage() );
			error.setElement( ApplyDGService.class.getCanonicalName() );
			
			return new ResponseEntity<Error>( error, HttpStatus.INTERNAL_SERVER_ERROR );
		} catch( Exception e ) {
			Error 	error	= new Error();
			error.setCode( HttpStatus.INTERNAL_SERVER_ERROR.toString() );
			error.setDescription( "INTERNAL_SERVER_ERROR: " + e.getMessage() );
			error.setElement( ApplyDGService.class.getCanonicalName() );
			
			return new ResponseEntity<Error>( error, HttpStatus.INTERNAL_SERVER_ERROR );
		}
	}
	
	@RequestMapping(  method = RequestMethod.GET, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE }, path = "/dgarantia" )
	public @ResponseBody ResponseEntity<?> getDGarantia
	(
		@RequestParam ( name = "id", required = true )		String id,
		@RequestHeader (name = "token", required = true)	String token
	)
	throws PrototypeException
	{
		try {
			ArrayList<DG>	dgs = new ArrayList<DG>();
			
			//LOAD FROM MEMORY IN ARRAYLIST
			if( DGs.getDGMap().isEmpty() ) {
				throw new PrototypeException( "NO HAY REGISTROS DE DEPÓSITOS EN GARANTÍA" );
			}
			if( DGs.getDGMap().containsKey( id ) ) {
				dgs.add( DGs.getDGMap().get( id ) );
			} else {
				throw new PrototypeException( "DEPÓSITO EN GARANTÍA NO ENCONTRADO" );
			}
			
			//RETURN ARRAYLIST DG
			return new ResponseEntity<ArrayList<DG>>( dgs, HttpStatus.OK );
		} catch( PrototypeException pE ) {
			Error 	error	= new Error();
			error.setCode( HttpStatus.INTERNAL_SERVER_ERROR.toString() );
			error.setDescription( "INTERNAL_SERVER_ERROR: " + pE.getMessage() );
			error.setElement( ApplyDGService.class.getCanonicalName() );
			
			return new ResponseEntity<Error>( error, HttpStatus.INTERNAL_SERVER_ERROR );
		} catch( Exception e ) {
			Error 	error	= new Error();
			error.setCode( HttpStatus.INTERNAL_SERVER_ERROR.toString() );
			error.setDescription( "INTERNAL_SERVER_ERROR: " + e.getMessage() );
			error.setElement( ApplyDGService.class.getCanonicalName() );
			
			return new ResponseEntity<Error>( error, HttpStatus.INTERNAL_SERVER_ERROR );
		}
	}
	
	@RequestMapping(  method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE }, path = "/dgarantia/query" )
	public @ResponseBody ResponseEntity<?> getQueryDGarantia
	(
		@RequestBody	( required = true )					ReportDGFilter	filter,
		@RequestHeader	(name = "token", required = true)	String			token
	)
	throws PrototypeException
	{
		try {
			ArrayList<DG>	dgsCache		= new ArrayList<DG>();
			ArrayList<DG>	dgsCacheF01		= new ArrayList<DG>();
			ArrayList<DG>	dgsCacheF02		= new ArrayList<DG>();
			ArrayList<DG>	dgsCacheF03		= new ArrayList<DG>();
			
			boolean			applyFilter01	= false;
			boolean			applyFilter02	= false;
			boolean			applyFilter03	= false;
			
			ArrayList<DG>	dgs 			= new ArrayList<DG>();
			
			//LOAD FROM MEMORY IN ARRAYLIST
			if( DGs.getDGMap().isEmpty() ) {
				throw new PrototypeException( "NO HAY REGISTROS DE DEPÓSITOS EN GARANTÍA" );
			}
			Collection<DG> colDGs = DGs.getDGMap().values();
			Iterator<DG> iDGs = colDGs.iterator();
			while( iDGs.hasNext() ) {
				dgsCache.add( iDGs.next() );
			}
			
			//FIRST
			if( filter.getUsername() == null || filter.getUsername().isEmpty() ) {
				//NO APPLY THIS FILTER
			} else {
				for( DG objDG : dgsCache ) {
					if( objDG.getUsername().equals( filter.getUsername() ) ){
						dgsCacheF01.add( objDG );
					}
				}
				applyFilter01 = true;
			}
			
			//SECOND
			if( filter.getRangeStart() == null ) {
				//NO APPLY THIS FILTER
			} else {
				if( filter.getRangeEnd() == null ) {
					//APPLY FILTER ONLY WITH START DATE IN RANGE
					if( applyFilter01 ) {
						for( DG objDG : dgsCacheF01 ) {
							if( ((filter.getRangeStart().getTime() - 1000) <= objDG.getDate().getTime()) && (objDG.getDate().getTime() <= (filter.getRangeStart().getTime() + 1000)) ){
								dgsCacheF02.add( objDG );
							}
							applyFilter02 = true;
						}
					} else {
						for( DG objDG : dgsCache ) {
							if( ((filter.getRangeStart().getTime() - 1000) <= objDG.getDate().getTime()) && (objDG.getDate().getTime() <= (filter.getRangeStart().getTime() + 1000)) ){
								dgsCacheF02.add( objDG );
							}
							applyFilter02 = true;
						}						
					}
				} else {
					if( filter.getRangeStart().getTime() <= filter.getRangeEnd().getTime() ) {
						//APPLY FILTER IN RANGE
						//DO IT
						if( applyFilter01 ) {
							for( DG objDG : dgsCacheF01 ) {
								if( ((filter.getRangeStart().getTime() - 1000) <= objDG.getDate().getTime()) && (objDG.getDate().getTime() <= (filter.getRangeEnd().getTime() + 1000)) ){
									dgsCacheF02.add( objDG );
								}
								applyFilter02 = true;
							}
						} else {
							for( DG objDG : dgsCache ) {
								if( ((filter.getRangeStart().getTime() - 1000) <= objDG.getDate().getTime()) && (objDG.getDate().getTime() <= (filter.getRangeEnd().getTime() + 1000)) ){
									dgsCacheF02.add( objDG );
								}
								applyFilter02 = true;
							}						
						}						
					}
				}
			}
			
			//THIRD
			if( filter.getStatus() == null || filter.getStatus().isEmpty() ) {
				//NO APPLY THIS FILTER
			} else {
				if( applyFilter02 ) {
					if( filter.getStatus().equals( "TODOS" ) ) {
						dgsCacheF03 = dgsCacheF02;
						applyFilter03 = true;
					} else {
						for( DG objDG : dgsCacheF02 ) {
							if( objDG.getStatus().equals( filter.getStatus() ) ){
								dgsCacheF03.add( objDG );
							}
						}
						applyFilter03 = true;
					}
				} else {
					if( applyFilter01 ) {
						if( filter.getStatus().equals( "TODOS" ) ) {
							dgsCacheF03 = dgsCacheF01;
							applyFilter03 = true;
						} else {
							for( DG objDG : dgsCacheF01 ) {
								if( objDG.getStatus().equals( filter.getStatus() ) ){
									dgsCacheF03.add( objDG );
								}
							}
							applyFilter03 = true;
						}
					} else {
						if( filter.getStatus().equals( "TODOS" ) ) {
							dgsCacheF03 = dgsCache;
							applyFilter03 = true;
						} else {
							for( DG objDG : dgsCache ) {
								if( objDG.getStatus().equals( filter.getStatus() ) ){
									dgsCacheF03.add( objDG );
								}
							}
							applyFilter03 = true;
						}
					}
				}
			}
			
			if( applyFilter03 ) {
				dgs = dgsCacheF03;
			} else {
				if( applyFilter02 ) {
					dgs = dgsCacheF02;
				} else {
					if( applyFilter01 ) {
						dgs = dgsCacheF01;
					} else {
						dgs = dgsCache;
					}
				}
			}
			
			//RETURN ARRAYLIST DG
			return new ResponseEntity<ArrayList<DG>>( dgs, HttpStatus.OK );
		} catch( PrototypeException pE ) {
			Error 	error	= new Error();
			error.setCode( HttpStatus.INTERNAL_SERVER_ERROR.toString() );
			error.setDescription( "INTERNAL_SERVER_ERROR: " + pE.getMessage() );
			error.setElement( ApplyDGService.class.getCanonicalName() );
			
			return new ResponseEntity<Error>( error, HttpStatus.INTERNAL_SERVER_ERROR );
		} catch( Exception e ) {
			Error 	error	= new Error();
			error.setCode( HttpStatus.INTERNAL_SERVER_ERROR.toString() );
			error.setDescription( "INTERNAL_SERVER_ERROR: " + e.getMessage() );
			error.setElement( ApplyDGService.class.getCanonicalName() );
			
			return new ResponseEntity<Error>( error, HttpStatus.INTERNAL_SERVER_ERROR );
		}
	}
	
	
}
