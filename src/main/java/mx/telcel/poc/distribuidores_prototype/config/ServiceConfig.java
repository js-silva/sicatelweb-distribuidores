package mx.telcel.poc.distribuidores_prototype.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import mx.telcel.poc.distribuidores_prototype.utils.Constants;


@Configuration
//@ComponentScan( "mx.telcel.poc.distribuidores_prototype.services" )
@ComponentScan( basePackages = { Constants.__SERVICE_BASE_SCAN__ } )
public class ServiceConfig {
	
}
